updateTime();

setInterval(function() {
  updateTime();
}, 1000);

function updateTime() {
  document.getElementById('time').innerHTML = new Date().toLocaleTimeString();
}